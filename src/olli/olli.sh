#!/usr/bin/env bash

BLUE="\e[1;38;5;39m"
GREEN="\e[1;38;5;70m"
RED="\e[1;38;5;124m"
YELLOW="\e[1;38;5;214m"
ORANGE="\e[1;38;5;202m"
NOFMT="\e[0m"

infomsg () {
    printf "$YELLOW[olli.sh]$NOFMT $1\n"
}

if [ $# -ne 1 ]; then
    infomsg "olli.sh requires one argument (the platform, i.e. linux or freebsd)"
    exit 1
fi

PLATFORM="$1"

if [ $PLATFORM != "linux" ]; then
    infomsg "olli.sh argument must be either linux or freebsd (only linux is supported at the moment)"
    exit 1
fi

OLLI_TL_TEMPLATE="https://gitlab.com/nicolas.hainaux/amoxtli/-/raw/master/src/olli/profiles/$PLATFORM.template"
INSTALLER_URL="http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz"

TL_YEAR=""
TL_BIN_DIR=""

# ON FREEBSD:
# perl5 is required to install texlive from script
# getopt and ghostscript9-base are required to run euktoeps

infomsg "wget $INSTALLER_URL"
wget $INSTALLER_URL
infomsg "tar xvzf install-tl-unx.tar.gz"
tar xvzf install-tl-unx.tar.gz
SOURCE_DIR=`ls | grep install-tl | grep -v unx`
infomsg "Found source directory: $SOURCE_DIR"

TL_YEAR=`echo $SOURCE_DIR | cut -c 12-15`
infomsg "Found TeXLive year: $TL_YEAR"
infomsg "curl $OLLI_TL_TEMPLATE > $SOURCE_DIR/texlive.profile"
curl $OLLI_TL_TEMPLATE > $SOURCE_DIR/texlive.profile
infomsg "sed -i \"s/__YEAR__/$TL_YEAR/g\" $SOURCE_DIR/texlive.profile"
sed -i "s/__YEAR__/$TL_YEAR/g" $SOURCE_DIR/texlive.profile
infomsg "cat $SOURCE_DIR/texlive.profile"
cat $SOURCE_DIR/texlive.profile
infomsg "sudo $SOURCE_DIR/install-tl -profile $SOURCE_DIR/texlive.profile"
sudo $SOURCE_DIR/install-tl -profile $SOURCE_DIR/texlive.profile
TL_BIN_DIR="/usr/local/texlive/$TL_YEAR/bin/x86_64-linux"
infomsg "printf $TL_BIN_DIR > $HOME/.tlbindir"
printf "$TL_BIN_DIR" > $HOME/.tlbindir
infomsg "export PATH=\"$TL_BIN_DIR:\$PATH\""
export PATH="$TL_BIN_DIR:$PATH"

# Already present in minimal scheme: amsfonts, textcomp
# Included in another package's name:
#     array, multicol: tools
#     graphicx: graphics
#     luatex85, lualatex-math, luaotfload: collection-luatex
# Required by eukleides: pstricks, pst-eps, moreverb, pst-tools
# Required for pdfjam: pdfjam pdfpages pdflscape
# Required to create slideshows: beamer extsizes
# Required by decimalcomma: was
PKG="cellspace sourcecodepro beamer extsizes fancyvrb xcolor lxfonts amsmath eurosym stackengine scalerel fontspec polyglossia siunitx pgf hyperref cancel placeins ulem tools graphics epstopdf textpos parskip geometry latex-bin epstopdf-pkg infwarerr grfext kvoptions pstricks pst-eps moreverb pst-tools collection-luatex pdfjam pdfpages pdflscape was decimalcomma"
infomsg "sudo env \"PATH=$PATH\" tlmgr install $PKG"
sudo env "PATH=$PATH" tlmgr install $PKG
# infomsg "ln -s "$TL_BIN_DIR"lualatex /usr/local/bin/lualatex"
# sudo ln -s "$TL_BIN_DIR"lualatex /usr/local/bin/lualatex
# infomsg "ln -s "$TL_BIN_DIR"luaotfload-tool /usr/local/bin/luaotfload-tool"
# sudo ln -s "$TL_BIN_DIR"luaotfload-tool /usr/local/bin/luaotfload-tool

# /!\ if ever need be: `sudo mktexlsr`
# (and not `mktexlsr`)

# FreeBSD: /!\ FONT CONFIG BELOW NOT DONE AND YET IT SEEMS TO WORK
# cp /usr/local/texlive/$TL_YEAR/texmf-var/fonts/conf/texlive-fontconfig.conf /etc/fonts/conf.d/09-texlive.conf
# fc-cache -fsv

# mktexlsr
