# minilib is a collection of small useful shell sh functions
# shared between home made scripts.
# It is intented to be sourced
# (e.g. imported in another script via . /path/to/minilib.sh)

BLUE="\e[1;38;5;39m"
GREEN="\e[1;38;5;70m"
RED="\e[1;38;5;124m"
YELLOW="\e[1;38;5;214m"
ORANGE="\e[1;38;5;202m"
NOFMT="\e[0m"
# Usage: printf "$BLUE""This text is blue$NOFMT and this one is normal\n"

get_cols () {
	COLS=`tput cols`
	SEP=" "

	for i in `jot $COLS | tr '\n' ' '`
	do
		SEP=$SEP"="
	done
}

# get1() echoes the first char of a string given as $1
get1 () {
	echo "$1" | cut -c1-1
}

msg () {
		get_cols
        echo
        text=$1
        len=`echo $1 | wc -c`
        if [ "$len" -lt "$COLS" ]; then
                text=`echo $1$SEP | cut -c -$COLS`
        fi
        echo $text
}

pause () {
	printf "Press any key to continue: "
	(   trap "stty $(stty -g;stty -icanon)" EXIT
	    LC_ALL=C dd bs=1 count=1 >/dev/null 2>&1
	    )   </dev/tty
#	(tty_state=$(stty -g)
#	stty -icanon
#	LC_ALL=C dd bs=1 count=1 >/dev/null 2>&1
#	stty "$tty_state"
#	) </dev/tty
}

# ask() will ask the yes/no question given as argument.
# The default answer (if user only types Enter) is
# considered as No, but it can be changed if the second
# is "Y", then the default answer is considered Yes.
# $1: the question
# $2: optional "Y" to make the default answer "YES"
YES_OR_NO=""
ask () {
	DEFAULT_YES="Y"
	DEFAULT_NO=""
	QUESTION="$1 [y/N] "
	if [ $# -eq 2 ] && [ $2 = "Y" ]; then
		DEFAULT_YES=""
		DEFAULT_NO="N"
		QUESTION="$1 [Y/n] "
	fi
	printf "$QUESTION"
	while :
	do
		read ANSWER
		case $ANSWER in
			[Yy][Ee][Ss]|[Yy]|$DEFAULT_YES)
				YES_OR_NO="YES"
				break
				;;
			[Nn][Oo]|[Nn]|$DEFAULT_NO)
				YES_OR_NO="NO"
				break
				;;
			*)
				echo "Sorry, I did not understand."
				echo -n "$QUESTION"
				;;
		esac
	done

}

# choose() will ask the question given as first argument.
# Each subsequent argument is treated as a possible answer.
# For instance: choose "Do you want to [i]nstall the file, [e]dit it or [c]ancel?" i e c
# will prompt the question as:
# Do you prefer to [i]nstall the file, [e]dit it or [c]ancel? [i/e/c]
CHOICE=""
choose () {
	QUESTION="$1 ["
	shift
	if [ $# -lt 2 ]
	then
		echo "At least two choices are required in addition to the question."
		return 1
	fi
	answers="$@"
	QUESTION="$QUESTION$1"
	shift
	for arg in "$@"
	do
		QUESTION="$QUESTION/$arg"
	done
	QUESTION="$QUESTION] "
	echo -n "$QUESTION"
	answered="false"
	while :
	do
		read CHOICE
		for a in $answers
		do
			if [ "$CHOICE" = "$a" ]
			then
				answered="true"
			fi
		done
		if [ "$answered" = "true" ]
		then
			break
		else
			echo "Sorry, I did not understand."
			echo -n "$QUESTION"
		fi
	done
	return 0
}

# Ask the user to confirm a provided value, else to provide the correct value instead
# $1 is the value that should be confirmed
# $2 is what it represents
# e.g. user_confirm xx.xx.xxx.www "the correct IP address"
# will ask "Is xx.xx.xxx.xxx the correct IP address?" and ask the user to enter the correct value if the user does not confirm.
USER_CONFIRMED=""
user_confirm () {
	USER_CONFIRMED="$1"
	while :
	do
		ask "Is $USER_CONFIRMED $2?"
		if [ $YES_OR_NO = "NO" ]; then
			printf "Please enter $2: "
			read USER_CONFIRMED
		else
			printf "$USER_CONFIRMED will be used as $2.\n"
			break
		fi
	done
}

# Replace pattern provided as $1 with string provided as $2 in file provided as $3.
# Only one occurence will be replaced.
# Optional $4 is set by default to 0. It defines replace()'s behaviour if the
# searched for pattern is not found. Meanings of $4:
# 0 or anything but 1 or 2 = if the pattern is not found, do nothing, echo nothing
# 1 = issue a warning
# 2 = issue an error and return 1
replace () {
	ending="0"
	if [ $# -ge 4 ]
	then
		ending="$4"
	fi
	found=`grep "$1" "$3"`
	if [ ! -z "$found" ]
	then
		sed -i.bak -E "s/$1/$2/" "$3" && rm "$3".bak
	else
		case $ending in
			1)
				echo "Warning: pattern $1 not found in file $3. Could not replace it by '$2'."
				;;
			2)
				echo "Error: pattern $1 not found in file $3. Could not replace it by '$2'."
				return 1
				;;
			*)
				;;
		esac
	fi
	return 0

}

# Insert $1 after the last line matching pattern provided as $2 in file provided as $3.
# $1 is the text what will be inserted directly. If one want to insert the content of a
# file, then add option --file at very first position.
# Optional $4 argument works the same way as the one of replace().
# Options -1 or --first, or -a or --all, or -L or --last can be used (must be
# written before any other argument, but after a possible --file) to insert only once
# after the first match, or after any match, or after the last match (default
# behaviour) respectively.
# CAUTION multiline patterns do not work; use working grep patterns;
# If you wish to insert a new line it does not seem possible directly;
# workaround to insert an empty line:
# touch empty.txt && insert --file empty.txt "pattern" dest_file.txt 1 && rm empty.txt
insert () {
	from_file=0
	if [ "$1" = "--file" ]
	then
		from_file=1
		shift
	fi
	direction="?"
	if [ `get1 "$1"` = '-' ]
	then
		case "$1" in
			"-1"|"--first")
				direction="/"
				;;
			"-a"|"--all")
				direction="all"
				;;
			"-L"|"--last")
				;;
			*)
				echo "Unkown option $1. Available options are -1 (--first), -a (--all) and -L (--last)."
				return 2
				;;
		esac
		shift
	fi
	ending="0"
	if [ $# -ge 4 ]
	then
		ending="$4"
	fi
	found=`grep "$2" "$3"`
	if [ ! -z "$found" ]
	then
		if [ "$from_file" = "1" ]
		then
			case "$direction" in
				"?"|"/")
					printf '%s\n' "$direction""$2""$direction""a" "`cat $1`" . x | ex -c 1 "$3"
					;;
				"all")
					sed -i.bak -E "/$2/r $1" "$3" && rm "$3.bak"
					;;
			esac
		else
			case "$direction" in
				"?"|"/")
					printf '%s\n' "$direction""$2""$direction""a" "$1" . x | ex -c 1 "$3"
					;;
				"all")
					sed -i.bak -E -e "/$2/a\\
$1" "$3" && rm "$3".bak
					;;
			esac
		fi
	else
		case "$ending" in
			"1")
				echo "Warning: pattern $2 not found in file $3. Could not insert '$1'."
				;;
			"2")
				echo "Error: pattern $2 not found in file $3. Could not insert '$1'."
				return 1
				;;
			*)
				;;
		esac
	fi
	return 0
}

# Delete any line matching pattern provided as $1 in file provided as $2.
delete () {
	sed -i.bak -E "/$1/d" "$2" && rm "$2.bak"
}

# Taken from https://www.cyberciti.biz/faq/repeat-a-character-in-bash-script-under-linux-unix/
# $1 is the number of times the string will be repeated
# $2 is the string to repeat
retell () {
	local start=1
	local end=${1:-80}
	local str="${2:-=}"
	local range=$(seq $start $end)
	for i in $range ; do printf "${str}"; done
}
