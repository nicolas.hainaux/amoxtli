#!/usr/bin/env bash

# This script automates releasing a new version of a python software:
# - to pypi (for python projects, requires pyproject.toml and poetry)
# - to launchpad for ubuntu packages (requires a debian/ directory)
# - to launchpad for debian packages (requires a debian/ directory, a debian git branch with the right setup)

# Examples:
# publi.sh minor --pypi --ppa focal --ppa jammy --ppa-debian jammy
# publi.sh --version 0.4.3 current --ppa focal --ppa jammy --ppa-debian jammy

source /usr/local/bin/minilib.sh

BLUE="\e[1;38;5;39m"
GREEN="\e[1;38;5;70m"
RED="\e[1;38;5;124m"
YELLOW="\e[1;38;5;214m"
ORANGE="\e[1;38;5;202m"
NOFMT="\e[0m"

LOCAL_BUILDS=$HOME/.local/builds
LOCAL_SHARE=$HOME/.local/share/publi.sh
LOCAL_TEMPLATES=$LOCAL_SHARE/templates

WD=`pwd`
NAME=`basename $WD`
EXT='conf'
CONFIG="$HOME/.config/publi.sh/$NAME.$EXT"
LATEST="$LOCAL_SHARE/latest_changes/$NAME.txt"
GIT_BRANCH="master"
# e.g. /home/nico/.config/publi.sh/cotinga.conf

W_NO_MASTER="$ORANGE""Warning:$NOFMT publishing should happen from master or main branch."
ERR_NO_CONFIG="$RED""Error:$NOFMT no config file found for $NAME (not found: $CONFIG)"
ERR_NO_POETRY="$RED""Error:$NOFMT versioning is not handled by poetry, cannot bump version"
ERR_NO_VERSION="$RED""Error:$NOFMT versioning is not managed by poetry. You must provide version number with option --version as first option on command line."
ERR_NO_GIT="$RED""Error:$NOFMT publi.sh must run at root of a git repo"
ERR_DEBIAN_BRANCH="$RED""Error:$NOFMT publi.sh cannot start to run in the debian branch. Please checkout another branch before starting."
ERR_NO_LATEST_CHANGES="$RED""Error:$NOFMT could not find a latest changes file for $NAME (should be $LATEST)"
MSG_EDIT_LATEST_CHANGES="Please edit the contents of $LATEST to fit your needs, then run publi.sh again."
USAGE1="Usage: publi.sh [--version VERSION] patch|minor|major|current [--pypi] [--ppa focal|jammy] [--ppa-debian focal|jammy]\n\n"
USAGE2="$BLUE[publi.sh]$NOFMT Option --version is for projects not managed by poetry. You can set the newer version number to use here. patch, minor or major cannot be used, then, you must use current.\n"
USAGE3="$BLUE[publi.sh]$NOFMT If a compliant CHANGELOG.rst is found, it will be proposed to save the latest changes for further use (automatically fill debian/changelog, for instance), and update it and commit the changes.\n"
USAGE4="$BLUE[publi.sh]$NOFMT If first argument is patch, minor or major, the version will be bumped accordingly.\n"
USAGE5="$BLUE[publi.sh]$NOFMT Use current to not bump to a newer version."
USAGE_MSG="$USAGE1$USAGE2$USAGE3$USAGE4$USAGE5"

infomsg () {
    printf "$BLUE[publi.sh]$NOFMT $1\n"
}

# maybe create $HOME/.config/publi.sh/ if it does not exist
if [ ! -f "$CONFIG" ]; then
    infomsg "$ERR_NO_CONFIG"
    exit 1
fi

source "$CONFIG"

# Check git repo (git status -> outputs fatal?; .git/ directory is there?)
# For python projects: check pyproject.toml is present, retrieve current version (see get_version below)

get_current_git_branch () {
    git rev-parse --abbrev-ref HEAD
}

get_version () {
    cat pyproject.toml | grep "^version = \"*\"" | cut -d'"' -f2
}

push_to_pypi () {
    if [ "$PYPI_USERNAME" = "" ]; then
        infomsg "$ORANGE""Warning:$NOFMT Ignoring --pypi option: PYPI_USERNAME being blank, publi.sh is unable to push to pypi.org"
        return 1
    fi
    infomsg "poetry build"
    poetry build
    infomsg "twine check $DIST_DIR/$NAME-$VERSION.tar.gz"
    twine check "$DIST_DIR/$NAME-$VERSION.tar.gz"
    if [ $? -eq 0 ]; then
        ask "$BLUE[publi.sh]$NOFMT Do upload to pypi?"
        if [ $YES_OR_NO = "YES" ]; then
            # poetry publish
            # --> does not work anymore (settings pb?); had to use
            twine upload --repository $NAME "$DIST_DIR/$NAME-$VERSION-py3-none-any.whl"
            # and see ~/.pypirc for settings relevant for twine
        fi
    else
        infomsg "$RED""Error:$NOFMT aborting: twine check did not return exit successfully. You can fix the problem and run publi.sh again."
        exit 1
    fi
}

# $1 is project's name
# $2 is series (focal, jammy...)
# $3 is version looked for
# $4 is distro (ubuntu or debian)
get_distro_iteration () {
    dump=`grep "$4" debian/changelog`
    if [ "$?" != 0 ]; then
        echo
    else
        grep "$1.*$3.*$4.*$2" debian/changelog | head -n 1 | cut -d" " -f2 | tr -d "()" | sed 's/^.*'"$4"'//' | sed 's/~'"$2"'$//'
    fi
}

# This assumes CHANGELOG.rst does exist and has newest changes starting by *
# between a line starting with Unreleased and another with Version
get_changelog_unreleased_content () {
    cat CHANGELOG.rst | sed -n '/Unreleased/,/Version/p' | grep "^\*"
}

update_changelog_rst () {
    RST_CONTENT=`get_changelog_unreleased_content`
    infomsg "Saving them to $LATEST..."
    printf "%s\n" "$RST_CONTENT" > "$LATEST"
    infomsg "Auto updating CHANGELOG.rst..."
    version_title="Version $VERSION (`date -I`)"
    version_title_length=${#version_title}
    version_underline=`retell "$version_title_length" '-'`
    # remove line next to Unreleased title (hence the underlining line)
    sed -i.bak -E '/^Unreleased$/{N;s/\n.*//;}' CHANGELOG.rst && rm CHANGELOG.rst.bak
    # insert underline, starting with a placeholder because dashes cause problems
    insert "__UNDERLINE_VERSION__" "^Unreleased$" CHANGELOG.rst 1
    replace "^__UNDERLINE_VERSION__$" "$version_underline" CHANGELOG.rst 1
    insert "$version_title" "^Unreleased$" CHANGELOG.rst 1
    # insert two new lines
    touch empty.txt && insert --file empty.txt "^Unreleased$" CHANGELOG.rst 1 && rm empty.txt
    touch empty.txt && insert --file empty.txt "^Unreleased$" CHANGELOG.rst 1 && rm empty.txt
    # insert underline, starting with a placeholder because dashes cause problems
    insert "__UNDERLINE_UNRELEASED__" "^Unreleased$" CHANGELOG.rst 1
    replace "^__UNDERLINE_UNRELEASED__$" "----------" CHANGELOG.rst 1
    while :
    do
        infomsg "git diff CHANGELOG.rst"
        git diff CHANGELOG.rst
        ask "$BLUE[publi.sh]$NOFMT Do stage these differences in CHANGELOG.rst for commit?"
        if [ $YES_OR_NO = "YES" ]; then
            git add CHANGELOG.rst
            break
        else
            printf "$BLUE[publi.sh]$NOFMT Please edit CHANGELOG.rst to fit your needs. "
            pause
        fi
    done
}

confirm_changes_for_debian () {
    while :
    do
        if [ ! -f "$LATEST" ]; then
            infomsg "$ERR_NO_LATEST_CHANGES"
            infomsg "$MSG_EDIT_LATEST_CHANGES"
            exit 1
        fi
        infomsg "A latest changes file has been found (there: $LATEST)."
        infomsg "Its content is:"
        cat "$LATEST"
        ask "$BLUE[publi.sh]$NOFMT Shall we use this content to update debian/changelog?"
        if [ $YES_OR_NO = "YES" ]; then
            break
        else
            printf "$BLUE[publi.sh]$NOFMT Please edit $LATEST to fit your needs."
            pause
        fi
    done
}

# $1 is the distro (ubuntu or debian)
update_debian_changelog () {
    DISTRO="$1"
    infomsg "Update debian/changelog:"
    DISTRO_ITERATION=`get_distro_iteration $PPA_PKG_NAME $SERIES $VERSION $DISTRO`
    if [ "$DISTRO_ITERATION" = "" ]; then
        DISTRO_ITERATION=0
    fi
    DISTRO_ITERATION=`printf "$DISTRO_ITERATION + 1\n" | bc`
    DISTRO_VERSION="$VERSION""-0$DISTRO$DISTRO_ITERATION"
    DISTRO_NAME="Ubuntu"
    if [ "$DISTRO" = "debian" ]; then
        DISTRO_NAME="Debian"
    fi
    infomsg "Newer $DISTRO_NAME version reads $GREEN$DISTRO_VERSION~$SERIES$NOFMT"
    cp $LOCAL_TEMPLATES/debian_changelog debian/changes
    replace "__PPA_PKG_NAME__" "$PPA_PKG_NAME" debian/changes
    replace "__DISTRO_VERSION__" "$DISTRO_VERSION" debian/changes
    replace "__SERIES__" "$SERIES" debian/changes
    replace "__SERIES__" "$SERIES" debian/changes
    replace "__AUTHOR__" "$PPA_AUTHOR" debian/changes
    replace "__AUTHOR_EMAIL__" "$PPA_AUTHOR_EMAIL" debian/changes
    replace "__DATE__" "`date -R`" debian/changes
    LATEST_REV=$LATEST".rev"
    tac $LATEST > $LATEST_REV
    while IFS=" " read -r line; do
    	insert "  $line" "__CONTENT__" debian/changes
    done < "$LATEST_REV"
    delete "__CONTENT__" debian/changes
    printf "%s\n" "`cat debian/changes; printf "\n\n" ; cat debian/changelog`" > debian/changelog
    while :
    do
        infomsg "git diff debian/changelog"
        git diff debian/changelog
        ask "$BLUE[publi.sh]$NOFMT Do you confirm this is correct?"
        if [ $YES_OR_NO = "YES" ]; then
            infomsg "git add debian/changelog"
            git add debian/changelog
            infomsg "git commit -m \"Update debian/changelog in order to publish $VERSION release on launchpad.net ($DISTRO_NAME $SERIES series).\""
            git commit -m "Update debian/changelog in order to publish $VERSION release on launchpad.net ($DISTRO_NAME $SERIES series)."
            break
        else
            printf "$BLUE[publi.sh]$NOFMT Please edit debian/changelog to fit your needs."
            pause
        fi
    done
    rm debian/changes
}

bump_version () {
    if [ $VERSIONING = "poetry" ]; then
        infomsg "poetry version $1"
        poetry version "$1"
        infomsg "git add pyproject.toml"
        git add pyproject.toml
    else
        infomsg "$ERR_NO_POETRY"
        exit 1
    fi
}

push_to_ppa () {
    SERIES="$1"
    DISTRO="$2"
    case "$SERIES" in
        "focal"|"jammy")
            update_debian_changelog "$DISTRO"
            infomsg "rm -rf $LOCAL_BUILDS/source/*"
            rm -rf "$LOCAL_BUILDS"/source/*
            BRANCH="$GIT_BRANCH"
            if [ "$DISTRO" = "debian" ]; then
                BRANCH="debian"
                infomsg "git checkout debian"
                git checkout debian
                infomsg "git merge $GIT_BRANCH -m \"Merged $GIT_BRANCH to debian\""
                git merge $GIT_BRANCH -m "Merged $GIT_BRANCH to debian"
                ask "$BLUE[publi.sh]$NOFMT Is everything allright? Answer yes to go on; or no to abort"
                if [ "$YES_OR_NO" = "NO" ]; then
                    infomsg "OK, aborting."
                    exit 1
                fi
                infomsg "git checkout $GIT_BRANCH"
                git checkout $GIT_BRANCH
            fi
            infomsg "git archive $BRANCH | tar -x -C $LOCAL_BUILDS/source"
            git archive "$BRANCH" | tar -x -C "$LOCAL_BUILDS"/source
            cd "$LOCAL_BUILDS"/source
            infomsg "debuild -k$PPA_KEY -S -i -I"
            debuild -k"$PPA_KEY" -S -i -I
            if [ $? -eq 0 ]; then
                FILE_TO_UPLOAD="$PPA_PKG_NAME"_"$DISTRO_VERSION~$SERIES""_source.changes"
                PPA_ID="$PPA_NAME"
                if [ "$DISTRO" = "debian" ]; then
                    PPA_ID="$PPADEBIAN_NAME"
                fi
                ask "$BLUE[publi.sh]$NOFMT Do upload $YELLOW$FILE_TO_UPLOAD$NOFMT to $PPA_ID?"
                if [ $YES_OR_NO = "YES" ]; then
                    infomsg "dput ppa:$PPA_ID $LOCAL_BUILDS/$FILE_TO_UPLOAD"
                    dput "ppa:$PPA_ID" "$LOCAL_BUILDS/$FILE_TO_UPLOAD"
                fi
            fi
            cd "$WD"
            ;;
        *)
            infomsg "$ORANGE""Warning:$NOFMT""Ignoring --ppa option: unsupported Ubuntu series: $SERIES"
            ;;
    esac
}

# MAIN SCRIPT STARTS AGAIN HERE ###############################################
if [ "$1" = '--help' ]; then
    infomsg "$USAGE_MSG"
    exit 0
fi
MSG1=""
MSG2=""
if [ "$VERSIONING" = "poetry" ]; then
    VERSION=`get_version`
    MSG1="Current version=$GREEN$VERSION$NOFMT"
else
    if [ "$1" = '--version' ]; then
        VERSION="$2"
        MSG1="Version number provided by command line: $GREEN$VERSION$NOFMT"
        shift
        shift
    else
        infomsg "$ERR_NO_VERSION"
        exit 1
    fi
fi

GIT_BRANCH=`get_current_git_branch`
if [ "$GIT_BRANCH" = "master" -o "$GIT_BRANCH" = "main" ]; then
    MSG2="working on branch $GREEN$GIT_BRANCH$NOFMT"
else
    MSG2="working on branch $ORANGE$GIT_BRANCH$NOFMT"
fi

infomsg "$MSG1; $MSG2"

if [ "$GIT_BRANCH" = "debian" ]; then
    infomsg "$ERR_DEBIAN_BRANCH"
    exit 1
fi

if [ "$GIT_BRANCH" != "master" -a "$GIT_BRANCH" != "main"  ]; then
    ask "$BLUE[publi.sh]$NOFMT $W_NO_MASTER Continue anyway?"
    if [ "$YES_OR_NO" = "NO" ]; then
        infomsg "OK, aborting."
        exit 1
    fi
fi

case "$1" in
			"patch"|"minor"|"major")
				bump_version "$1"
				;;
			"current")
				;;
			*)
                infomsg "$RED""Error:$NOFMT expected command patch|minor|major|current"
                infomsg "$USAGE_MSG"
                exit 1
				;;
esac
# remove $1 (patch|minor|major|current)
shift

if [ "$VERSIONING" = "poetry" ]; then
    VERSION=`get_version`
fi

if [ -f "CHANGELOG.rst" ]; then
    RST_CONTENT=`get_changelog_unreleased_content`
    if [ "$RST_CONTENT" = "" ]; then
        infomsg "Found a CHANGELOG.rst file, but could not detect unreleased changes in CHANGELOG.rst."
    else
        infomsg "Found following unreleased changes in CHANGELOG.rst:"
        printf "$RST_CONTENT\n"
        infomsg "It is possible to save them as latest changes in $LATEST"
        infomsg "(for further use in debian/changelog, for instance), and automatically update CHANGELOG.rst"
        ask "$BLUE[publi.sh]$NOFMT Do you wish to do that and stage the changes of CHANGELOG.rst for the new release commit?"
        if [ "$YES_OR_NO" = "YES" ]; then
            update_changelog_rst
        fi
    fi
fi

if [ "$VERSIONING" = "poetry" ]; then
    VERSION=`get_version`
    infomsg "Version is now $GREEN$VERSION$NOFMT"
fi

STAGED_FILES=`git diff --staged`

if [ "$STAGED_FILES" != "" ]; then
    infomsg "git diff --staged"
    git diff --staged
    ask "Do you confirm these changes should be committed as new release?"
    if [ "$YES_OR_NO" = "YES" ]; then
        infomsg 'git commit -m "Release '"$VERSION"'"'
        git commit -m "Release $VERSION"
        infomsg "git tag -a $VERSION -m \"Version $VERSION\""
        git tag -a "$VERSION" -m "Version $VERSION"
    else
        infomsg "Please fix the situation manually, then run publi.sh again"
        exit 1
    fi
fi

CHANGES_CONFIRMED_FOR_DEBIAN=false

while [ $# -gt 0 ]; do
    case "$1" in
        "--pypi")
            push_to_pypi
		    ;;
        "--ppa"|"--ppa-debian")
            if [ "$1" = "--ppa-debian" ]; then
                infomsg "Using --ppa-debian assumes there is a git branch named debian and that it contains the correct setup to create a package specifically for debian, e.g. the debian/rules file overrides dh_builddeb with dh_builddeb -- -Zgzip"
                infomsg "Also, the config file $CONFIG must define a PPADEBIAN_NAME variable in order for this to work."
            fi
            if ! $CHANGES_CONFIRMED_FOR_DEBIAN; then
                confirm_changes_for_debian
                CHANGES_CONFIRMED_FOR_DEBIAN=true
            fi
            distro="ubuntu"
            if [ "$1" = "--ppa-debian" ]; then
                distro="debian"
            fi
            push_to_ppa "$2" "$distro"
            # two extra shifts below, because when using --ppa or --ppa-debian,
            # two values are required
            shift
            shift
            ;;
        *)
            infomsg "$USAGE_MSG"
            exit 1
            ;;
    esac
    shift
done
