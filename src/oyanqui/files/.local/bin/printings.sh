#!/usr/bin/env bash

# TODO: mettre ça dans une fonction et faire une boucle de sorte à afficher direct toutes les fiches
# triées par niveau ; ajouter un argument lessons ou tests pour avoir les leçons ou les tests, depuis
# le même répertoire (faire cd .évals/ pour choper les évals)

ls */*aménagée*.pdf */*EX\ *.pdf | grep -vi CORRIGÉ | grep -vi réponses | grep -vi transpas | grep -vi projeter | grep -vi projeté | cut -f2 -d"/"

# pour des évals aller dans [3456]e/.évals/ puis :
# ls -d */**ÉVAL*.pdf | grep -vi "TRANSPA" | grep -vi "CORRIGÉ" | grep -vi "OLD" | grep -vi "réponses" | cut -f2 -d"/"
