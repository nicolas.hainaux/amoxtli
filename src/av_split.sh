#!/bin/env bash

# Split audio or video files with ffmpeg

# Each line is assumed to be like this: hh:mm:ss hh:mm:ss Title
# They will be numbered automatically.

SCHEME="./scheme"

MEDIA_FILE="$1"
ext="${MEDIA_FILE##*.}"

# TODO: add test to check the MEDIA_FILE exists

declare -i nb=1

while IFS=" " read -r line; do
    start=`echo "$line" | cut -d' ' -f1`
    end=`echo "$line" | cut -d' ' -f2`
    title=`echo "$line" | cut -d' ' -f3-`
    n=`printf '%02d' $nb`
    echo "READ LINE $n: start=$start; end=$end; title=$title"
    ffmpeg -ss "$start" -to "$end" -i "$MEDIA_FILE" -acodec copy -vcodec copy "$n $title.$ext"
    nb+=1
done < $SCHEME
